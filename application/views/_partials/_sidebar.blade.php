<?php 
defined('BASEPATH') OR exit('No direct script access allowed');?>

<div id="sidebar-wrapper">
      <ul id="sidebar_menu" class="sidebar-nav">
       <li class="sidebar-brand"><a href="{{ base_url('welcome/sources') }}">CI4</a></li>
     </ul>
     <ul class="sidebar-nav" id="sidebar">
      <li>
        <a href="{{ site_url('pages/panel') }}">Fourre-tout<span class="fa-stack fa-lg pull-left"><i class="fa fa-chevron-circle-right fa-stack-1x "></i></span></a>
      </li>
      <li>
        <a href="{{ site_url('blog_c') }}">Blog<span class="fa-stack fa-lg pull-left"><i class="fa fa-pencil-square-o fa-stack-1x"></i></span></a>
      </li>
      <li data-toggle="collapse" data-target="#media" class="collapsed">
        <a href="#">Médias<span class="fa-stack fa-lg pull-left"><i class="fa fa-chevron-circle-down fa-stack-1x "></i></span></a>
        <ul class="sub-menu collapse" id="media">
          <li>
            <a href="{{ base_url('welcome/videos') }}">Vidéos</a>
          </li>
          <li>
            <a href="{{ base_url('welcome/photos') }}">Portfolios</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{ base_url('welcome/contact') }}">Contact<span class="fa-stack fa-lg pull-left"><i class="fa fa-flag fa-stack-1x "></i></span></a>
      </li>
      <li>
        <a data-toggle="modal" href="{{ base_url('') }}#myModal"><span class="fa-stack fa-lg pull-left"><i class="fa fa-sign-in fa-stack-1x "></i></span>Login</a>
      </li>
    </ul>
    <ul class="sidebar-nav sidebar-bottom">
      <li> 
        <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-facebook fa-stack-1x "></i></span></a>
        <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-twitter fa-stack-1x "></i></span></a>
        <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-linkedin fa-stack-1x "></i></span></a>
        <a href="https://github.com/EnRodage/Blade-Codeigniter-3"><span class="fa-stack fa-lg pull-left"><i class="fa fa-github fa-stack-1x "></i></span></a>
      </li> 
    </ul>
  </div>
<!-- /#sidebar-wrapper -->

<!--login modal-->
<div id='myModal' class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Login</h4>
          </div>
          <div class="modal-body">
      <form class="form-horizontal" action="{{ site_url('admin/login/login') }}" method="post" role="form">
                <label>Pseudo</label>
                  <div class="row">
                  	<div class="col-md-8 col-md-offset-2">
                  		<input type="text" class="form-control" id="username" name="username" placeholder="Username">
                  	</div>
                  </div>
                  <label>Mot de passe</label>
                  <div class="row">
                  	<div class="col-md-8 col-md-offset-2">   
                      <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                  	</div>
                  </div>
          </div><!-- modal-body -->
                  <div class="modal-footer">
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Fermer</button>
                      <input type="submit" class=" btn btn-primary" value="Login">
                      </div>
      </form>
                  </div>
           
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->