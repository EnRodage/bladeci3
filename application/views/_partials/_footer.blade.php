<?php 
defined('BASEPATH') OR exit('No direct script access allowed');?>


@yield('js')
<!-- Bootstrap and Bootcards JS -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="{{ base_url('assets/js/custom.js') }}"></script>
