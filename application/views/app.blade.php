<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="fr">
<head>
    @include('_partials/_head')
</head>
<body>
    @include('_partials/_sidebar')
<div id="wrapper">
    <div class="container-fluid">
        <div id="page-content-wrapper">
            <div class="container-fluid xyz">
                <div class="row">
                    <div class="col-lg-12">
                        @yield('content')
                    </div><!--col-12-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#wrapper -->

@include('_partials/_footer')


</body>
</html>