jQuery(document).ready(function(){
	if (jQuery("#myTabP a#show-versions").length > 0 && jQuery(".ayaslider").length > 0 && jQuery(".version").length > 1){
		jQuery('.prev').show();
		jQuery('.next').show();

		jQuery(document).on("shown.bs.tab", "#myTabP a#show-versions", function(e){
			jQuery(".ayaslider").ayaSlider({
				easeIn : 'easeOutBack',
				easeOut : 'linear',
				delay : 0,
				timer : "",
				previous : jQuery('.prev'),
				next : jQuery('.next'),
				list : ""
			});
		});
	}

	if (jQuery(".package-card-inner a").length > 0){
		jQuery(".package-card-inner a").click(function(e){
			e.stopPropagation();
		});
	}

	if (jQuery(".package-card-inner").length > 0){
		jQuery(".package-card-inner").click(function(e){
			e.preventDefault();
			var link = jQuery(this).find("a:first").attr("href");
			jQuery(location).attr("href", link);
		});
	}

	if (jQuery(".select-all").length > 0){
		jQuery(".select-all").on("click", function(){
			this.select();
		});
	}

	if (jQuery(".bs-docs-sidenav").length > 0){
		if (jQuery(".bs-docs-sidenav").length > 0){
			setTimeout(function () {
				jQuery(".bs-docs-sidenav").affix({
					offset: {
						top: function () { return jQuery(window).width() <= 980 ? 290 : 250 },
						bottom: 270
					}
				})
			}, 100)
		}
	}

	if (jQuery(".nav-tabs").length > 0){
		var parts = decodeURI(window.location.href).split('#');
		jQuery('.nav-tabs a[href="#'+parts[1]+'"]').tab("show");
	}

	if (jQuery("#packages-grid, #packages-list, #packages-full-list").length > 0){
		var packages = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			limit: 20,
			remote: base_url+"packages/list/%QUERY"
		});

		packages.initialize();

		jQuery("#search-package").typeahead({
			minLength: 1,
			highlight: true,
		}, {
			name: "packages",
			displayKey: "name",
			source: packages.ttAdapter(),
			templates: {
			suggestion: Handlebars.compile([
				'<a href="'+base_url+'packages/package/{{maintainer}}/{{name}}">',
				'<p class="package-name">{{maintainer}}/{{name}}</p>',
				'<p class="package-description">{{description}}</p>',
				'</a>'
				].join(''))
			}
		});

		/*jQuery("#search-package").keyup(function(){
			if (jQuery(this).val() != ""){
				jQuery("#packages-list .package-card").removeClass("search-visible");
				jQuery("#packages-list .package-card:contains-ci('"+jQuery(this).val()+"')").addClass("search-visible");
			}else{
				jQuery("#packages-list .package-card").addClass("search-visible");
			}
			jQuery("#packages-list").isotope({ filter:".search-visible"});
			console.log(jQuery("#packages-list .search-visible").length);
			if (jQuery("#packages-list .search-visible").length <= 0){
				jQuery("#no-package").show();
			}else{
				jQuery("#no-package").hide();
			}
		});*/

		// jQuery("#packages-list").tablesorter({
            // headers: {
                // 3: {
                    // sorter: "ints"
                // }
            // }
        // });
	}

	if (jQuery("#packages-full-list").length > 0){
		/*jQuery("#search-package").keyup(function(){
			if (jQuery(this).val() != ""){
				jQuery("#packages-full-list tbody>tr").hide();
				jQuery("#packages-full-list td:contains-ci('"+jQuery(this).val()+"')").parent("tr").show();
			}else{
				jQuery("#packages-full-list tbody>tr").show();
			}
		});*/

		jQuery("#packages-full-list").tablesorter({
            headers: {
                3: {
                    sorter: "ints"
                }
            }
        });
	}

	if (jQuery("#requests-list").length > 0){
		jQuery("#search-request").keyup(function(){
			if (jQuery(this).val() != ""){
				jQuery("#requests-list tbody>tr").hide();
				jQuery("#requests-list td:contains-ci('"+jQuery(this).val()+"')").parent("tr").show();
			}else{
				jQuery("#requests-list tbody>tr").show();
			}
		});

		jQuery("#requests-list").tablesorter({
            headers: {
                3: {
                    sorter: "ints"
                }
            }
        });
	}

	if (jQuery(".markitup").length > 0){
		jQuery(".markitup").markItUp(mySettings);
	}
});

jQuery.extend(jQuery.expr[":"], {
	"contains-ci": function(elem, i, match, array) {
		return (elem.textContent || elem.innerText || jQuery(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	}
});

jQuery.tablesorter.addParser({
	id: "ints",
	is: function(s) {
		return false; 
	},
	format: function(s) {
		return $.tablesorter.formatFloat(s.replace(" ", ""));
	},
	type: "numeric"
});